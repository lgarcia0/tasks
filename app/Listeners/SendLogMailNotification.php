<?php

namespace App\Listeners;

use App\Events\NewLogAdded;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Mail;

class SendLogMailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewLogAdded  $event
     * @return void
     */
    public function handle(NewLogAdded $event)
    {
        $data = $event->data;
        $user = $data['task']->user;

        Mail::send('mails.logs', ['data'=> $data], function($message) use ($user) {
        
            $message->from('noreply@tasks.com', 'Task system')
                    ->to($user->email, $user->name)
                    ->subject('New log');
        });
    }
}
