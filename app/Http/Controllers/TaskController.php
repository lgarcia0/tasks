<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();
        $today = Carbon::now();

        foreach ($tasks as $key => $task) {
            $max_execution_date  = new Carbon($task->max_execution_date);
            $task->expire = (Carbon::parse($today->format('Y-m-d h:i:s'))->diffInMinutes($max_execution_date->format('Y-m-d h:i:s'), false) < 0) ? true : false;
        }

        return view('task.index', ['tasks' => $tasks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('task.create')->with(['users' => User::where('id','!=', Auth::user()->id)->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'assigned_to' => 'required',
            'description' => 'required',
            'max_execution_date' => 'required|date',
        ]);
           
        $data = $request->all();

        Task::create([
            'description' => $data['description'],
            'max_execution_date' => $data['max_execution_date'],
            'assigned_to' => $data['assigned_to'],
            'user_id' => Auth::user()->id,
        ]);


        return redirect("task")->withSuccess('Task created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        if ($task->assigned_to == Auth::user()->id) {
            return view('task.show')->with(['task' => $task]);
        }

        return redirect("task")->withError('Task is not assigned to you');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
