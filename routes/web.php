<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TaskController;
use App\Http\Controllers\LogController;

use App\Models\Invoice;
use App\Models\Product;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Desafio 1
Route::get('/desafio1', function () {

    $invoice = Invoice::select(DB::raw('Sum(quantity * price) AS total'))
                     ->join('products', 'invoices.id', '=', 'products.invoice_id')
                     ->get();

    $invoices = Product::select(DB::raw('distinct(invoice_id) as ids'))
                            ->where('quantity', '>', 100)
                            ->get();

    $products = Product::select('name')
                         ->whereRaw('quantity * price > ?', [1000000])
                         ->get();

    return response()->json([
        'total' => $invoice[0]->total,
        'invoces' => $invoices->pluck('ids'),
        'products' => $products->pluck('name')  
    ]);
});

//Desafio 3
Route::get('/product', function () {

    $product = Product::create([
        'invoice_id' => 2,
        'name' => 'Product 4',
        'quantity' => 200,
        'price' => 500000
    ]);

    return response()->json([
        'product' => $product
    ]);

});

Auth::routes();

Route::get('/', function(){
    return redirect("task");
})->name('home');

Route::resource('task', TaskController::class);
Route::resource('log', LogController::class);
