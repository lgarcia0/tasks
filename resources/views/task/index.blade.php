@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="d-flex w-100 justify-content-between pb-2">
                <h3>List</h3>
                <a href="{{ route('task.create') }}" class="btn btn-primary">Add task</a>
            </div>
            @if (isset($tasks) && count($tasks) > 0) 
                <div class="list-group">
                    @foreach ($tasks as $task)
                        <a href="{{ route('task.show', $task) }}" @class(['list-group-item', 'list-group-item-action', 'danger' => $task->expire ])>
                            <div class="mb-1 px-2">
                                <div class="py-3" style="min-height: 56px; font-size:12px">
                                    <p class="mb-0">Created by {{ $task->user->name }} </p>
                                    <p class="mb-0"> {{ \Carbon\Carbon::parse($task->created_at)->format('j F, Y H:m') }}</p>
                                </div>
                                <div class="d-flex w-100">
                                    <p class="lead text-truncate">{{ $task->description }}</p>
                                </div>
                                <div class="" style="font-size:12px">
                                    <span class="text-info"> Expire: {{ \Carbon\Carbon::parse($task->max_execution_date)->format('j F, Y H:m') }}</span>   
                                    <span class="float-right">Assigned to: {{ $task->assigned->name }}</span>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            @else
                <div>No data available</div>
            @endif
        </div>
    </div>
@endsection