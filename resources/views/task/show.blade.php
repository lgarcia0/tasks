@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-md-6 py-4" style="background: #eee; border-radius: 5px">  
        <div class="mb-1 px-2">
            <div class="py-3" style="min-height: 56px; font-size:12px">
                <p class="mb-0">Created by {{ $task->user->name }} </p>
                <p class="mb-0"> {{ \Carbon\Carbon::parse($task->created_at)->format('j F, Y H:m') }}</p>
            </div>
            <div class="d-flex w-100">
                <p class="lead">{{ $task->description }}</p>
            </div>
            <div class="" style="font-size:12px">
                <p class="text-info"> Expire: {{ \Carbon\Carbon::parse($task->max_execution_date)->format('j F, Y H:m') }}</p>   
            </div>
        </div>
        <hr>
            
        <form action="{{ route('log.store') }}" method="post">
            @csrf
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Add a comment" name="comment" aria-label="Add a comment" aria-describedby="button-addon2" required>
                @error('comment')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <input type="hidden" id="task_id" name="task_id" value="{{ $task->id }}">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-secondary" type="button" id="button-addon2">Comment</button>
                </div>
            </div>
        </form>
        <ul class="list-group d-flex w-100">
            @foreach ($task->logs as $log)
                <li class="p-2 mb-1" style="list-style:none;background: #ded8d863;border-radius:5px">
                    <p class="lead px-2" style="font-size:14px; ">{{ $log->comment }}</p>
                    <p class="mb-1 float-right" style="font-size:10px; "> {{ \Carbon\Carbon::parse($log->created_at)->format('j F, Y H:m') }}</p>
                </li>
            @endforeach
        </ul>
    </div>
</div>
@endsection