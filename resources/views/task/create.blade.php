@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <form action="{{ route('task.store') }}" method="post" enctype="multipart/form-data">
        @csrf
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" placeholder=""></textarea>
                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror   
            </div>
            <div class="form-group">
                <label for="assigned_to">Assigned to</label>
                <select class="custom-select @error('assigned_to') is-invalid @enderror" name="assigned_to">
                    <option selected value="">Select</option>
                    @foreach ($users as $user)
                        <option value="{{$user->id}}">{{ $user->name }}</option>
                    @endforeach
                </select>
                @error('assigned_to')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="max_execution_date">Expire</label>
                <input type="datetime-local" class="form-control @error('max_execution_date') is-invalid @enderror" name="max_execution_date" id="max_execution_date">
                @error('max_execution_date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection