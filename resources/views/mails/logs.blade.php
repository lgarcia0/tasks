<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name'), 'Tasks' }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!-- Styles -->
    <style>
    body{
        background-color: #f3f3f3;
        font-family: 'Roboto', sans-serif;
    }
    .card {
      margin: 0 auto; /* Added */
      float: none; /* Added */
      margin-bottom: 10px; /* Added */
    }
    .cabecera{
        padding: 20px 40px;
        background-color: #0075BD;
        z-index: 999;
        text-align:center;
        width: 500px;
        margin: 20px auto 0px auto; /* Added */
        float: none; /* Added */
    }
    .container-center{
        padding: 20px 40px;
        background-color: #ffffff;
        width: 500px;
        margin: 0 auto; /* Added */
        float: none; /* Added */
        margin-bottom: 10px; /* Added */
    }
    .rounded{ border-radius: 8px 8px 0px 0px; }
    .middle{ text-align: center; }
    .img{
        width: auto;
        height: 150px;
        border-radius: 8px;
    }
    </style>
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body>
    <div class="container-center">
        <div>
            <p><b>Log </b> <span>{{ $data['log'] }}</span></p>
        </div>
    </div>
</body>
</html>
