<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class InvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('invoices')->insert([
            [
                'user_id' => 1,
                'seller_id' => 2,
                'type' => 'Sale',
                'total' => 240000,
                'created_at' => Carbon::now()
            ],
            [
                'user_id' => 1,
                'seller_id' => 2,
                'type' => 'Sale',
                'total' => 200000,
                'created_at' => Carbon::now()
            ],

        ]);
    }
}
