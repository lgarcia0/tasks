<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('products')->insert([
            [
                'invoice_id' => 1,
                'name' => 'Product 1',
                'quantity' => '120',
                'price' => 1000,
                'created_at' => Carbon::now()
            ],
            [
                'invoice_id' => 1,
                'name' => 'Product 2',
                'quantity' => '120',
                'price' => 1000,
                'created_at' => Carbon::now()
            ],
            [
                'invoice_id' => 2,
                'name' => 'Product 2',
                'quantity' => '200',
                'price' => 1000,
                'created_at' => Carbon::now()
            ],

        ]);
    }
}
